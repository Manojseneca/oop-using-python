from abc import ABC, abstractmethod


# Interfaces
class IProduct(ABC):
    @abstractmethod
    def get_price(self) -> float:
        pass


class IOrder(ABC):
    @abstractmethod
    def add_product(self, product: IProduct) -> None:
        pass
    
    @abstractmethod
    def remove_product(self, product: IProduct) -> None:
        pass
    
    @abstractmethod
    def calculate_total_price(self) -> float:
        pass


class ICustomer(ABC):
    @abstractmethod
    def get_name(self) -> str:
        pass
    
    @abstractmethod
    def get_address(self) -> str:
        pass

    
class IShipper(ABC):
    @abstractmethod
    def ship_order(self, order: IOrder, customer: ICustomer) -> None:
        pass
    

# Concrete classes
class Product(IProduct):
    def __init__(self, name: str, price: float):
        self.name = name
        self.price = price
    
    def get_price(self) -> float:
        return self.price


class Order(IOrder):
    def __init__(self):
        self.products = []
        
    def add_product(self, product: IProduct) -> None:
        self.products.append(product)
        
    def remove_product(self, product: IProduct) -> None:
        self.products.remove(product)
        
    def calculate_total_price(self) -> float:
        return sum(product.get_price() for product in self.products)


class Customer(ICustomer):
    def __init__(self, name: str, address: str):
        self.name = name
        self.address = address
        
    def get_name(self) -> str:
        return self.name
    
    def get_address(self) -> str:
        return self.address

    
class Shipper(IShipper):
    def __init__(self, name: str):
        self.name = name
        
    def ship_order(self, order: IOrder, customer: ICustomer) -> None:
        print(f"Shipping order to {customer.get_name()} at {customer.get_address()} via {self.name}")
