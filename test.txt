Test that a product can be created with a name and a price, and that its price can be retrieved.
Test that an order can be created, products can be added to it, and its total price can be calculated correctly.
Test that a customer can be created with a name and an address, and that their name and address can be retrieved.
Test that a shipper can be created with a name, and that an order can be shipped to a customer using the shipper.
Test that derived classes can be used interchangeably with their base class, for example, creating a derived class of Product that adds a discount, and adding it to an order.
Test that high-level modules depend on abstractions, not on concrete implementations, for example, creating a function that takes an IOrder and an IShipper as arguments and uses them to ship the order.